screengen [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](https://godoc.org/gitlab.com/opennota/screengen?status.svg)](http://godoc.org/gitlab.com/opennota/screengen) [![Pipeline status](https://gitlab.com/opennota/screengen/badges/master/pipeline.svg)](https://gitlab.com/opennota/screengen/commits/master)
=========

A library for generating screenshots from video files, and a command-line tool for generating thumbnail grids.

## Install the package

    go get -u gitlab.com/opennota/screengen

## Install the command-line tool

    go install gitlab.com/opennota/screengen/cmd/screengen@latest

## Also

* There is a [similar program](https://github.com/OlegKochkin/screengen) by that name.

* For a more feature-rich command-line thumbnail grid generator (which also doesn't require ImageMagick) see [mutschler/mt](https://github.com/mutschler/mt).

## Donate

**Bitcoin (BTC):** `1PEaahXKwJvNJGJa2PXtPFLNYYigmdLXct`

**Ethereum (ETH):** `0x83e9607E693467Cb344244Df10f66c036eC3Dc53`
